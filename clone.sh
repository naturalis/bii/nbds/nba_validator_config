#!/bin/sh
# clone.sh : clone gitlab repo containing the nba validator configuration
# and move taht to the required directory: /config
#
# NOTE: the branch name of the git repo, serves as the tag name of the
# docker image.

# pull configuration repo
cd /repo || (echo "error: failed to cd into /repo" && exit 1)
rm -R ./* 2> /dev/null
if [ -z "${BRANCH}" ];
then
  echo "info: checking out main branch of the validator configuration"
  if ! git clone "${REPO}" > /dev/null 2>&1
  then
    echo "error: failed to clone the main branch of the repository ${REPO}"
    exit 1
  fi
else
  echo "info: checking out branch ${BRANCH} of the validator configuration"
  if ! git clone "${REPO}" --branch "${BRANCH}" > /dev/null 2>&1
  then
    echo "error: failed to clone the ${BRANCH} branch of the repository ${REPO}"
    exit 1
  fi
fi

# move configuration to /config directory
cd nba_validator_config || (echo "error: failed to cd to /nba_validator_config" && exit 1)
rm -R /config/* 2> /dev/null
mv config/* /config/.
cd /repo && rm -R nba_validator_config/

echo "info: updated validator configuration"
