FROM alpine:3
LABEL maintainer="tom.gilissen@naturalis.nl"

WORKDIR /
ARG REPO=""
ARG BRANCH=""
ENV REPO="${REPO}"
ENV BRANCH="${BRANCH}"

RUN mkdir -p /config
RUN mkdir -p /repo

RUN apk --no-cache add git
COPY clone.sh .

CMD [ "/clone.sh" ]
# CMD [ "tail", "-f", "/dev/null" ]